var PDFDocument = require('pdfkit'),
	_ = require('underscore'),
	http = require('http'),
	child_process = require('child_process'),
	fs = require('fs'),
	xml2js = require('xml2js');

var app = {
	methods : {}
};


app.start = function(){
	app.doc = new PDFDocument({
		size : [210, 180],
		margin : 0
	});
	app.methods.weather();
	app.methods.news();
	//app.write();
	//app.save();
	//app.print();
};

app.save = function(){
	app.docId = Math.random().toString().substring(2,8);
	app.doc.write(app.docId + '.pdf');
	app.print();
	//fs.unlink(app.docId + '.pdf');
};

app.print = function(){
	child_process.exec('lp -d Epson -o media=Custom.210x200 ' + app.docId + '.pdf', function(err, out){
		if(err){
			return console.log("There was an error while Printing");
		}
		console.log("Printing…")
	})
};

app.write = function(){
	var test = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed pulvinar diam eu ' +
        'dolor bibendum et varius diam laoreet. Morbi eget rutrum enim. Sed enim ipsum, ' +
        'posuere nec hendrerit non, commodo quis tortor. Fusce id nisl augue. Fusce at ' +
        'lectus ut libero vehicula imperdiet.';

      app.doc.text(new Date());
      app.doc.text(test);
};

app.methods.welcome = function(){
	var n = new Date(),
		hour = n.getHours(),
		dateString = n.getHours() + ':' + n.getMinutes(),
		weather =  app.weather.current_observation.weather,
		str;


	if(hour < 11){

		str = 'Hey, Good Morning, its ';
		
	} else if( hour < 14){

		str = 'Hey Whats up, its ';

	} else if( hour < 18){

		str = 'Hey, Good Afternoon, its ';
	} else {

		str = 'Hey, Good Evening, its ';
	}

	str += dateString + ' and the Weather is ' + weather + ' outside.';

	app.doc.font('pacifico.ttf');

	app.doc.text(str, {
		align : 'center',
	}).moveDown(1);
};

app.methods.hn = function(){
	httpGet('news.ycombinator.com', '/rss', function(d){
		var parser = new xml2js.Parser();
		parser.addListener('end', function(result) {
		    console.dir(result.channel.item);
		});
		
		parser.parseString(d);
		
	});
};

app.methods.news = function(){
	httpGet('www.spiegel.de', '/schlagzeilen/tops/index.rss', function(d){
		var parser = new xml2js.Parser();
		parser.addListener('end', function(result) {
		    console.dir(result.channel.item);
		});
		
		parser.parseString(d);
		
	});
};



app.methods.weather = function(){
	httpGet('api.wunderground.com', '/api/4d348014c58c8f1b/conditions/forecast/q/zmw:00000.3.10738.json', function(d){
		var data = JSON.parse(d);

		app.weather = data;

		app.methods.welcome();
		app.writeHeadline('Forecast');
			
		data.forecast.txt_forecast.forecastday.forEach(function(forecast, i){
			if(i >= 3) return;

			app.doc.font('Courier').fontSize(9).text(forecast.title.toUpperCase());
			app.resetStyle();
			app.doc.text(forecast.fcttext).moveDown(0.2);
		});

		app.save();
	});
};

app.resetStyle = function(){
	app.doc.font('Times-Roman').fontSize(9);

};

app.writeHeadline = function(txt){

	app.doc.font('Courier').fontSize(10);

	app.doc.text('o--------------------------------o');

	app.doc.text("~ " + txt +" ~", {
		align: 'center'
	});

	app.doc.text('o--------------------------------o').moveDown(0.5)

	app.doc.font('Times-Roman').fontSize(9);

};

function httpGet(url, path, cb){
	var body = "",
		req = http.request({
		  host: url,
		  port: 80,
		  path : path

		}, function(res) {
		  res.setEncoding('utf8');
		  res.on('data', function (chunk) {
		   body += chunk;
		  });
		  res.on("end", function(){
		  	cb(body);
		  });

		});

		req.end();
}
app.start();


